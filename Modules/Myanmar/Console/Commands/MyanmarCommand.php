<?php

namespace Modules\Myanmar\Console\Commands;

use Illuminate\Console\Command;

class MyanmarCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:MyanmarCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Myanmar Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Command::SUCCESS;
    }
}
