@extends('frontend.layouts.app')

@section('title') {{ __($module_title) }} @endsection

@section('content')

<div class="myanmar">
    <section class="myanmar__destination position-relative">
        <img class="myanmar__destination--banner w-100" src="{{asset('img/Frame 483183.png')}}" alt="Frame 483183">
        <div class="myanmar__destination--info container position-absolute start-50 translate-middle top-50 pt-5">
            <div class="row align-items-center">
                <div class="col-md-5 col-12 mb-3">
                    <span class="wt-font-merriweather wt-fw-300 fst-italic wt-fz-18 wt-color-gray">Destination</span>
                    <div class="d-flex mb-4 mt-3">
                        <p class="pr-3 wt-font-merriweather wt-fw-400 wt-fz-48 wt-color-gray">Myanmar</p>
                        <img src="{{asset('img/image 33 (3).png')}}" alt="image 33">
                    </div>
                    <div class="d-flex flex-column">
                        <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                        <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm">Myanmar is a sacred land, which has praised the Buddhism for thousand years, therefore, it seems like wherever we go in this country, we still find the Buddhist pagodas, even if it is a huge pagoda in the big city, or a delicate small pagoda hidden in wild nature</p>
                    </div>
                </div>
                <div class="col-md-7 col-12">
                    <div class="row images">
                        <div class="col-12 mb-3">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 2 (1).png')}}" alt="">
                        </div>
                        <div class="col-6">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 3 (1).png')}}" alt="">
                        </div>
                        <div class="col-4">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 6.png')}}" alt="">
                        </div>
                        <div class="col-2 position-relative">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 6.png')}}" alt="">
                            <div class="overlay">
                                <p class="d-flex justify-content-center align-items-center"><span class="text-white wt-fz-20 wt-fw-600">+10</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="myanmar__content container py-md-5 py-4">
        <div class="d-flex flex-column align-items-center justify-content-center mb-5 pb-4">
            <div class="myanmar__content--description d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">Sacred pagodas and enormous towers, golden roofs glittering in the sun and iridescent longyi skirts are all the things that will evoke everyone about an extraordinary country of South East Asia – the wonderful Myanmar.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">Myanmar is a sacred land, which has praised the Buddhism for thousand years, therefore, it seems like wherever we go in this country, we still find the Buddhist pagodas, even if it is a huge pagoda in the big city, or a delicate small pagoda hidden in wild nature. This country is also famous as the Land of Golden pagodas. Particularly, most of the pagodas and temples of this country are covered in precious gold, and decorated with twinkling jewels and diamonds, which makes it glisten in the dazzling sun. That has become an outstanding symbol of this country.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">Although it has numerous vast Buddhist constructions, the natural landscape of this country is not ruined. In Myanmar, we could still explore the wild and spectacular scenery, like the great Inle Lake where we could go hiking, or the overlapping mountainous sector of Mrak-Ou and Zwekabin for climbing.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">In the past, Myanmar was under the rule of British Colonists for almost ninety years, however, the Burmese had never lost their own unique identity and traditions. The people of Myanmar are adored because of their kindness, hard work, friendliness, and especially, they always have a great belief in Buddha, and follow Buddha’s teachings to live beautifully. Nowadays, the traditional handicrafts like cigars making, lacquer ware, or basketry crafting have still existed and developed. That offers travelers a chance to visit and experience the product procedure and the daily life of local people here.</p>
            </div>
        </div>
        <div class="w-100 position-relative wt-line-gradient"></div>
    </section>
    <section class="myanmar__favorite container p-md-5 p-3">
        <div class="d-flex flex-column justify-content-center align-items-center mb-5">
            <img src="{{asset('img/map.png')}}" alt="">
            <div class="myanmar__favorite--controls mt-4 d-sm-flex justify-content-center">
                <span data-slide="0" class="active item-1 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Yangon</span>
                <span data-slide="1" class="item-2 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Kyaikto</span>
                <span data-slide="2" class="item-3 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Bagan</span>
                <span data-slide="3" class="item-4 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Mandalay</span>
                <span data-slide="4" class="item-5 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Inle Lake</span>
            </div>
        </div>
        <div class="myanmar__favorite--slider position-relative">
            <div class="slide">
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/yangon.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Yangon</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/kyaikto.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Kyaikto</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/bagan.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Bagan</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/mandalay.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Mandalay</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/inle.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Inle Lake</p>
                    </div>
                </div>
            </div>
            <button class="prev-btn position-absolute bottom-50">
                <i class="fa-solid fa-angle-left"></i>
            </button>
            <button class="next-btn position-absolute bottom-50">
                <i class="fa-solid fa-angle-right"></i>
            </button>
        </div>
        <div class="myanmar__favorite--des mt-5">
            <div data-slide="0" class="d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Yangon is the largest city of Myanmar which is one of the most famous destination of this country.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">This city is dazzled by the golden Buddhist pagodas. You could find a great deal of pagodas in Yangon, namely Chaukhtatgyi Pagoda, Kaba Aye Pagoda which is also known as World Peace Pagoda, or the Sule pagoda which was built 2200 year ago. The most vital monastery of Yangon is Shwedagon Pagoda which is one of the wonders of the world. It was built 2500 years ago. This pagoda is well-known as the place restoring the hair relics of Buddha, which is related to a myth that the Buddha had gifted his eight strings of hair to two brothers, and now they are kept and saved at this holy place.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Located about 3 miles from the place of these pagodas are two peaceful lakes, named Inya and Kandawgyi, where we could walk leisurely and lend your hair down.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Even though it is the sanctuary of Buddhist pagodas, this city still has a lively lifestyle with a bustling local market. Bogyoke Aung San Market is the most famous market of this city, where we could buy local handicrafts, souvenirs and experience the daily life of local people.</p>
            </div>
            <div data-slide="1" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">About 200 kilometers to the south from Yangon, we could transfer easily from Yangon to this town by the road to continue discovering this sacred Buddhist country.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Golden Rock is an odd stone lying on the cliffside of mountain. According to the myth, this rock has been being there for over 2500 years without any moving. This destination sticks with a legend that Buddha had gifted a strand of his hair to a hermit, and then this hermit required the King dominating this country at this time, who was described as the son of Gods to save this strand under a rock whose shape is like the hermit’s head. The Kind found this mountain as the most suitable place, and then used his supernatural power to build this golden stone, and put the strand under. This stone is shining aurorally, because it is covered with a great number of gold leaves.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Kyaik Htee Yoe is the pagoda built on the top of the Golden Rock. This pagoda is constructed following the typical style of Burmese pagodas, with the tall and twist-shaped roof. Looking from the far distance, this complex looks really eye-catching with the luminous shades of gold.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">This is an extremely famous destination for Buddhist pilgrims. However, the pilgrimage at this place is for male only, females are not allowed to attend. The male pilgrims will surpass a bridge crossing over an abyss to reach the pagoda and the stone, and fasten a gold leaf on the stone with all their veneration.</p>
            </div>
            <div data-slide="2" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Located in central Myanmar, Bagan is an ancient city, which was designated as The World Heritages by UNESCO in 2019.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Lying serenely next to the Ayeyarwady river which is the longest river of Myanmar, the culture of Bagan has been founded and developed in hundred years. We could easily see that there are numerous pagodas and temples all over this city, which were all built from 11th to 13th century. Specifically, the most famous monasteries are Shwezigon Pagoda which was built in 11th century, the Ananda Temple which is eulogized as the most sophisticated temple of Bagan, or the massive brick Dhammayangyi Temple. These sacred places are close to this country’s religion, culture and history, which are the best places to visit if you would like to discover the uniqueness of this spiritual country.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Besides the temples and pagodas, this city is also widely known for its peace and magnificent landscape. The most popular natural place to visit in Bagan is the Popa Mount. This volcano, which has not been active anymore, has played an important role in the culture of Bagan, because it has existed along with the Bagan’s history. It is also the shrine to worship 37 nats, as well as Burmese ancient animist spirits.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">One of the most exciting activities in Bagan is taking the hot air balloon to have a panoramic view of Bagan in the air. From the above, all the splendid natural sceneries, the quiet, mysterious pagodas glowing under the sun will be kept in your eyes. That will definitely be the most overwhelming and passionate feeling in your entire life.</p>
            </div>
            <div data-slide="3" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Mandalay hill – where we could challenge ourselves with trekking or climbing – panoramic view to the city and neighboring area.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Mandalay is the second largest city of Myanmar, which was the ancient royal capital city of the Konbaung dynasty, the sovereign of King Mindon in 1857. When the King founded this imperial capital, he hoped that Mandalay could be the place keeping all the priceless values of Buddhism, and spreading this quintessence to the descendants, to avoid losing them in the invasion of the British Empire.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Considered as the cradle of Buddhism, this city has numerous relics of temples and pagodas with exotic architecture. Mandalay has the Kuthodaw Pagoda – a white pagodas exhibiting stone tablets engraved with the Tripitaka by the original Theravada Pali language, built by King Mindon to preserve the Buddhist values. Moreover, this city also has other beautiful monasteries, namely Kyauktawgyi Pagoda, Mahamuni temple or the world’s largest bronze bell called Mingun where we could discover more about the culture and Buddhist of Myanmar.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">The landscape and infrastructure of Mandalay also offers us with other exciting activities. We could visit the U Bein bridge – the longest wooden bridge in the world located in the outskirts of Mandalay, which has always been acclaimed as the greatest place to see the sunset in Myanmar. We could also go up the Mandalay hill to enjoy the panoramic view of the whole peaceful and sacred city.</p>
            </div>
            <div data-slide="4" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">When someone asks which destination could not be missed in Myanmar, the answer would definitely be the Inle Lake. Inle Lake is a great lake, which is praised as the heart of Shan state.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Like other places in Myanmar, we still find the magnificent gilded Buddhist pagodas with typical architecture of pagodas in Myanmar. Wherever you go, Myanmar will never lose the unique look of a Buddhist country.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">However, Inle Lake attracts the wanderers the most by its floating villages and gardens where we could come and experience the daily life of local people. Strolling along the Indein or Na Daung Kya villages, visiting the local markets, we would feel as if we were a part of the living there. This place is also the living area of the ethnic groups and tribes, which will give us a chance to discover more about their unique culture and tradition.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">The local people living in the Inle Lake area still preserve their traditional handicrafts. By visiting here, we could see and join the traditional jobs like Rice Cracker making, typical cigars “Cheeroots” producing, or learn how to make the silk from lotus plants and so on.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Besides the calm lifestyle of people, the natural landscape at Inle Lake is also really charming with a gentle clear lake surrounded by the verdant hills. Therefore, simply enjoying the fresh air, hiking by the lake, or taking the magnificent pictures to keep the memories of the trip are also the best things to do in this destination.When someone asks which destination could not be missed in Myanmar, the answer would definitely be the Inle Lake. Inle Lake is a great lake, which is praised as the heart of Shan state.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Like other places in Myanmar, we still find the magnificent gilded Buddhist pagodas with typical architecture of pagodas in Myanmar. Wherever you go, Myanmar will never lose the unique look of a Buddhist country.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">However, Inle Lake attracts the wanderers the most by its floating villages and gardens where we could come and experience the daily life of local people. Strolling along the Indein or Na Daung Kya villages, visiting the local markets, we would feel as if we were a part of the living there. This place is also the living area of the ethnic groups and tribes, which will give us a chance to discover more about their unique culture and tradition.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">The local people living in the Inle Lake area still preserve their traditional handicrafts. By visiting here, we could see and join the traditional jobs like Rice Cracker making, typical cigars “Cheeroots” producing, or learn how to make the silk from lotus plants and so on.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Besides the calm lifestyle of people, the natural landscape at Inle Lake is also really charming with a gentle clear lake surrounded by the verdant hills. Therefore, simply enjoying the fresh air, hiking by the lake, or taking the magnificent pictures to keep the memories of the trip are also the best things to do in this destination.</p>
            </div>
        </div>
        <div class="myanmar__favorite--control-other my-5 px-md-5">
            <div class="w-100 pt-3 px-3 d-flex justify-content-between">
                <button class="prev wt-font-merriweather fw-bold wt-fz-14 px-3 py-2 wt-rounded-6" type="button"><i class="fa-solid fa-arrow-left-long pr-2"></i> Previous</button>
                <button class="next wt-font-merriweather fw-bold wt-fz-14 px-3 py-2 wt-rounded-6" type="button">Next <i class="fa-solid fa-arrow-right-long pl-2"></i></button>
            </div>
        </div>
    </section>
</div>

@endsection
