<?php

namespace Modules\OurService\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class OurService extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'ourservices';

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return \Modules\OurService\database\factories\OurServiceFactory::new();
    }
}
