<?php

namespace Modules\Vietnam\Console\Commands;

use Illuminate\Console\Command;

class VietnamCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:VietnamCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Vietnam Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Command::SUCCESS;
    }
}
