<?php

namespace Modules\Lao\database\seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Tag\Models\Lao;

class LaoDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        /*
         * Laos Seed
         * ------------------
         */

        // DB::table('laos')->truncate();
        // echo "Truncate: laos \n";

        Lao::factory()->count(20)->create();
        $rows = Lao::all();
        echo " Insert: laos \n\n";

        // Enable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
