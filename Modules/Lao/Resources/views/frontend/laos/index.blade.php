@extends('frontend.layouts.app')

@section('title') {{ __($module_title) }} @endsection

@section('content')

<div class="laos">
    <section class="laos__destination position-relative">
        <img class="laos__destination--banner w-100" src="{{asset('img/Frame 483183.png')}}" alt="Frame 483183">
        <div class="laos__destination--info container position-absolute start-50 translate-middle top-50 pt-5">
            <div class="row align-items-center">
                <div class="col-md-5 col-12 mb-3">
                    <span class="wt-font-merriweather wt-fw-300 fst-italic wt-fz-18 wt-color-gray">Destination</span>
                    <div class="d-flex mb-4 mt-3">
                        <p class="pr-3 wt-font-merriweather wt-fw-400 wt-fz-48 wt-color-gray">Laos</p>
                        <img src="{{asset('img/image 33 (2).png')}}" alt="image 33">
                    </div>
                    <div class="d-flex flex-column">
                        <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                        <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm">Laos still has its own unique attractions with unspoiled nature, giant mountains, cryptic caves and caverns, as well as clear and majestic waterfalls.</p>
                    </div>
                </div>
                <div class="col-md-7 col-12">
                    <div class="row images">
                        <div class="col-12 mb-3">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 2 (1).png')}}" alt="">
                        </div>
                        <div class="col-6">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 3 (1).png')}}" alt="">
                        </div>
                        <div class="col-4">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 6.png')}}" alt="">
                        </div>
                        <div class="col-2 position-relative">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 6.png')}}" alt="">
                            <div class="overlay">
                                <p class="d-flex justify-content-center align-items-center"><span class="text-white wt-fz-20 wt-fw-600">+10</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="laos__content container pt-md-5 pt-4">
        <div class="d-flex flex-column align-items-center justify-content-center mb-5 pb-4">
            <div class="laos__content--description d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">Accompanying Vietnam and Cambodia, Laos is also a country located in the Indochina peninsula, which creates a union of 3 countries on this peninsula.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">Unlike Vietnam which has a long eastern beach and Cambodia has the Sihanoukville with a beautiful ocean, Laos is a land locked country. It shares the border with Vietnam, China, Myanmar, Thailand and Cambodia. However, Laos still has its own unique attractions with unspoiled nature, giant mountains, cryptic caves and caverns, as well as clear and majestic waterfalls.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">This country is affected infinitely by Buddhism, which has been considered as the national religion of Laos for many years. That is the reason why it is not difficult to come across the golden colossal Buddhist pagodas with huge brown fish scale tile roofs, which is Laos traditional architecture. These pagodas have been considered as typical symbols of this country.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">Similar to other two nations in the Indochina Peninsula, Laos also suffered a time of being a colony, and war to defend the country’s peace. It caused people of this country to have lion-hearted characteristics and bravery to confront the difficulties. Nevertheless, after getting over all of the hardships, they are still preserving their original manners – honest, truthful and warm-hearted.</p>
            </div>
        </div>
        <div class="w-100 position-relative wt-line-gradient"></div>
    </section>
    <section class="laos__favorite container p-md-5 p-3">
        <div class="d-flex flex-column justify-content-center align-items-center mb-5">
            <img src="{{asset('img/map.png')}}" alt="">
            <div class="laos__favorite--controls mt-4 d-flex justify-content-center">
                <span data-slide="0" class="active item-1 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Luang Prabang</span>
                <span data-slide="1" class="item-2 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Vientiane</span>
                <span data-slide="2" class="item-3 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Vang Vieng</span>
            </div>
        </div>
        <div class="laos__favorite--slider position-relative">
            <div class="slide">
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/luang.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Luang Prabang</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/vientiane.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Vientiane</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/vang-vieng.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Vang Vieng</p>
                    </div>
                </div>
            </div>
            <button class="prev-btn position-absolute bottom-50">
                <i class="fa-solid fa-angle-left"></i>
            </button>
            <button class="next-btn position-absolute bottom-50">
                <i class="fa-solid fa-angle-right"></i>
            </button>
        </div>
        <div class="laos__favorite--des mt-5">
            <div data-slide="0" class="d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Luang Prabang’s location is in the northern area of Laos, where two rivers named Mekong and Namkhan spin around. This place had been the ancient royal capital of Laos until 1975, where the Laotian Royal Family used to live.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Spreading the traditional breath of an ancient capital, Luang Prabang draws people’s attention by the imperial constructions, outstandingly the Haw Kham, as known as the Royal Palace, where were the residence of King Sisavang Vong and his family. Moreover, this place is famous with hundred-year sacred pagodas, like the Wat Xiengthong pagoda – one of the highest Buddhist symbols of Laos, the oldest pagoda of Luang Prabang, as well as Wat Sene, Wat Visoun, Wat Mai or Wat Aham.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Luang Prabang is also an ancient capital protected by the great natural wall. Clasping by the spectacular mountains and winding long rivers, Luang Prabang offers travelers a chance to adapt with nature. Climbing up the Phousi mountain to challenge yourself and admire Luang Prabang panoramic view from the peak, discovering the Pak Ou cave by sitting on boat and flowing along the Mekong river, cool off at Kuangsi Water Fall, where is famous by not only the magnificent scenery but also the ethnic minority living around and their traditional handicraft, or visit Tad Sae waterfall where you can experience elephant riding.</p>
            </div>
            <div data-slide="1" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Being located in the south of Laos, Vientiane had been the metropolis of Lan Xang Kingdom – the previous Laos since 1573, and then an important city during the French colonial time. Vientiane became the capital city of new Independent Laos in 1953, existed parallelly with the royal capital Luang Prabang until 1975, and then became the only capital city of Laos.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Vientiane is the city having a great deal of Buddhist marks. In other words, in Vientiane, no matter where you come, you could easily find a Buddhist places or pagodas. One of the most famous and essential places is That Luang, which is a Buddhist gilded tower, considered as the icon the of Laos, Wat Sisaket where owns thousands of miniature Buddha statues, or the Buddha Park, where exhibits giant Buddha statues imported from India, which would impress you with the cleverness of sculptors.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Under the gentle shelter of Buddhism, this city looks really serene. That would be really suitable for those who are looking for peace, or would like to do spiritual tourism.</p>
            </div>
            <div data-slide="2" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Neither an important city like Vientiane, nor a big province like Luang Prabang, Vang Vieng is just a small town. This city’s position is on the road between Luang Prabang and Vientiane, which is extremely suitable for transportation, and it is also called as a destination that we could not miss when we visit Laos.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">This small town has a special geography. It is surrounded by treacherous limestone mountains, fanciful caves and caverns, as well as the Nam Song river flowing in front of the town, which create an excellent topography that the Feng Shui called “laying back on mountain, treading foot on water”. That is such a lucky symbol that this place could develop brilliantly.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Vang Vieng is the most ideal place for those who are passionate about ecotourism. Coming to Vang Vieng, we could explore the famous caves like Tham Phoukham Cave or Tham Jang Cave, kayaking on Nam Song river to discover the landscape and caverns, or swimming in the Blue Lagoon, which looks like the color of a precious emerald.</p>
            </div>
        </div>
        <div class="laos__favorite--control-other my-5 px-md-5">
            <div class="w-100 pt-3 px-3 d-flex justify-content-between">
                <button class="prev wt-font-merriweather fw-bold wt-fz-14 px-3 py-2 wt-rounded-6" type="button"><i class="fa-solid fa-arrow-left-long pr-2"></i> Previous</button>
                <button class="next wt-font-merriweather fw-bold wt-fz-14 px-3 py-2 wt-rounded-6" type="button">Next <i class="fa-solid fa-arrow-right-long pl-2"></i></button>
            </div>
        </div>
    </section>
</div>

@endsection
