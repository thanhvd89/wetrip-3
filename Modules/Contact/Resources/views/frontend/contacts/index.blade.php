@extends('frontend.layouts.app')

@section('title') {{ __($module_title) }} @endsection

@section('content')

<div class="contact-us">
    <section class="contact-us__info container p-md-5 p-3">
        <div class="d-flex flex-column justify-content-center align-items-center mb-3">
            <p class="wetrip wt-font-merriweather wt-fw-300 fst-italic wt-color-gray wt-rounded-10 px-3 py-1">Wetrip Travel</p>
            <h4 class="wt-font-antonio wt-fw-400 wt-fz-48 wt-color-blue mt-4 mb-5">Contact Us</h4>
        </div>
        <div class="w-100 d-flex justify-content-center">
            <div class="line"></div>
        </div>
        <div class="contact-us__info--affiliate mt-5">
            <h5 class="wt-font-merriweather wt-fw-400 wt-fz-30 wt-color-gray text-center mb-4 pb-2 pt-3">
                Our Affiliate
            </h5>
            <div class="type row">
                <div class="col-lg-3 col-sm-6 col-12 d-flex justify-content-center wt-cursor">
                    <img class="my-3" src="{{asset('img/image 17.png')}}" alt="image 17">
                </div>
                <div class="col-lg-3 col-sm-6 col-12 d-flex justify-content-center wt-cursor">
                    <img class="my-3" src="{{asset('img/image 18.png')}}" alt="image 18">
                </div>
                <div class="col-lg-3 col-sm-6 col-12 d-flex justify-content-center wt-cursor">
                    <img class="my-3" src="{{asset('img/image 19.png')}}" alt="image 19">
                </div>
                <div class="col-lg-3 col-sm-6 col-12 d-flex justify-content-center wt-cursor">
                    <img class="my-3" src="{{asset('img/image 20.png')}}" alt="image 20">
                </div>
            </div>
        </div>
    </section>
    <section class="contact-us__contact container p-md-5 p-4">
        <div class="row">
            <div class="col-sm-10 col-9">
                <div class="row">
                    <div class="col-lg-6 col-12 mb-4">
                        <h5 class="mb-5 pb-4 wt-font-merriweather fw-bold wt-fz-32">Let’s Get in Touch</h5>
                        <div class="row mb-4">
                            <div class="col-lg-12 col-6 mb-3">
                                <h6 class="text-uppercase wt-fz-14 wt-fw-500 wt-color-gray mb-2">Australia</h6>
                                <p class="wt-fz-14 wt-fw-400 wt-color-gray mb-2">2 Pinewood Ave, Kardinya, WA 6163, Australia</p>
                                <p class="wt-fz-14 wt-fw-400 wt-color-gray mb-2">079 5567 2982</p>
                            </div>
                            <div class="col-lg-12 col-6 mb-3">
                                <h6 class="text-uppercase wt-fz-14 wt-fw-500 wt-color-gray mb-2">Vietnam</h6>
                                <p class="wt-fz-14 wt-fw-400 wt-color-gray mb-2">207 Cau Giay, Hanoi, Vietnam</p>
                                <p class="wt-fz-14 wt-fw-400 wt-color-gray mb-2">(+84) 941 960 024  - (+84) 941 960 124</p>
                            </div>
                        </div>
                        <div class="d-flex">
                            <i class="fa-regular fa-envelope wt-color-blue"></i>
                            <p class="pl-3 wt-fz-16 wt-fw-400 wt-color-black">contact@wetrip.vn</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mb-4">
                        <div class="form row">
                            <div class="col-md-6 col-12 mb-4">
                                <input class="w-100" type="text" placeholder="Your name">
                            </div>
                            <div class="col-md-6 col-12 mb-4">
                                <input class="w-100" type="text" placeholder="Email address">
                            </div>
                            <div class="col-12 mb-4">
                                <input class="w-100" type="text" placeholder="Contact phone">
                            </div>
                            <div class="col-12 mb-5">
                                <textarea class="w-100" type="text" placeholder="Your comment"></textarea>
                            </div>
                            <div class="col-12 mb-4">
                                <button type="button" class="wt-bg-blue d-flex align-items-center text-white"><p class="wt-fz-14 wt-fw-400 p-3">SUBMIT MESSAGE</p> <i class="fa-solid fa-arrow-right-long p-3"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 col-3 d-flex justify-content-end">
                <div class="type d-flex flex-column px-4">
                    <img class="py-4" src="{{asset('img/Group 143.png')}}" alt="Group">
                    <img class="py-4" src="{{asset('img/Group 71.png')}}" alt="Group">
                    <img class="py-4" src="{{asset('img/icon.png')}}" alt="icon">
                    <img class="py-4" src="{{asset('img/facebook icon.png')}}" alt="facebook">
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
