@extends('frontend.layouts.app')

@section('title') {{ __($module_title) }} @endsection

@section('content')

<div class="cambodia">
    <section class="cambodia__destination position-relative">
        <img class="cambodia__destination--banner w-100" src="{{asset('img/Frame 483183.png')}}" alt="Frame 483183">
        <div class="cambodia__destination--info container position-absolute start-50 translate-middle top-50 pt-5">
            <div class="row align-items-center">
                <div class="col-md-5 col-12 mb-3">
                    <span class="wt-font-merriweather wt-fw-300 fst-italic wt-fz-18 wt-color-gray">Destination</span>
                    <div class="d-flex mb-4 mt-3">
                        <p class="pr-3 wt-font-merriweather wt-fw-400 wt-fz-48 wt-color-gray">Cambodia</p>
                        <img src="{{asset('img/image 33 (1).png')}}" alt="image 33">
                    </div>
                    <div class="d-flex flex-column">
                        <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                        <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm">Cambodia is located in the basin of the Mekong River, which is one of the longest rivers of Asia. That is why you could see an immense web of rivers around this country.</p>
                    </div>
                </div>
                <div class="col-md-7 col-12">
                    <div class="row images">
                        <div class="col-12 mb-3">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 2 (1).png')}}" alt="">
                        </div>
                        <div class="col-6">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 3 (1).png')}}" alt="">
                        </div>
                        <div class="col-4">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 6.png')}}" alt="">
                        </div>
                        <div class="col-2 position-relative">
                            <img class="w-100 wt-rounded-20" src="{{asset('img/image 6.png')}}" alt="">
                            <div class="overlay">
                                <p class="d-flex justify-content-center align-items-center"><span class="text-white wt-fz-20 wt-fw-600">+10</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cambodia__content container pt-md-5 pt-4">
        <div class="d-flex flex-column align-items-center justify-content-center mb-5 pb-4">
            <div class="cambodia__content--description d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">Cambodia Kingdom is widely known as the Country of ancient sacred temples and towers. Previously, this country was the territory of Khmer Empire, which is one of the most brilliant majesty in the South East Asia area, with numerous heritages existing until today. The Khmer Empire’s main religion was Hinduism, and then gradually turned into Buddhism in the 12th century. As a result, it is not difficult to find the durable relics of Hinduism temples and towers, which was built conscientiously by the clever ancient Khmer. The long living of these constructions has been always causing wonders to the descendants like us about the methods that ancestors using to complete colossal edifices. That means when we visit Cambodia, we will touch our little finger to the enormous mystery of humanity.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">Cambodia is located in the basin of the Mekong River, which is one of the longest rivers of Asia. That is why you could see an immense web of rivers around this country. And moreover, Cambodia is also famous with its concave topography, which makes the Tonle Sap – a branch stream of Mekong River looks like a spectacular ocean without shorelines. All of these features create a signature landscape for this country – the scene of floating markets, as well as the rural local villages located on the river banks.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center lh-base mb-3">A river can turn the land where it runs though into a cultural area. This statement comes from historians, and it has always been exact. Thousand years that the Mekong river crosses Cambodia’s land is also the time that people living in this country have existed, developed and preserved their own traditional values. Like other nations in the area, the Cambodian has surpassed the lost and pain. They had a period of being the colony of France, and after a short time being a central nation, they were once again under the cruel domination and rule of Polpot, which caused a lot of tragic sacrifices of innocent citizens over 40 years ago. Leaving all the pain behind, nowadays, the Cambodian have always been happy, hard-working, honest and pleased to welcome people from all around the world to visit their country.</p>
            </div>
        </div>
        <div class="w-100 position-relative wt-line-gradient"></div>
    </section>
    <section class="cambodia__favorite container p-md-5 p-3">
        <div class="d-flex flex-column justify-content-center align-items-center mb-5">
            <img src="{{asset('img/map.png')}}" alt="">
            <div class="cambodia__favorite--controls mt-4 d-flex justify-content-center">
                <span data-slide="0" class="active item-1 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Siem Reap</span>
                <span data-slide="1" class="item-2 d-inline-block mb-2 px-3 py-2 wt-rounded-6 fw-bold wt-font-merriweather wt-fz-18 wt-cursor">Phnom Penh</span>
            </div>
        </div>
        <div class="cambodia__favorite--slider position-relative">
            <div class="slide">
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/siem.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Siem Reap</p>
                    </div>
                </div>
                <div class="slider-item position-relative">
                    <img class="w-100 h-100" src="{{asset('img/penh.png')}}" alt="">
                    <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                        <h5 class="wt-fz-20 wt-fw-600 text-white mb-2 text-uppercase">Location 1</h5>
                        <p class="wt-fw-300 wt-fz-14 wt-color-yellow">Phnom Penh</p>
                    </div>
                </div>
            </div>
            <button class="prev-btn position-absolute bottom-50">
                <i class="fa-solid fa-angle-left"></i>
            </button>
            <button class="next-btn position-absolute bottom-50">
                <i class="fa-solid fa-angle-right"></i>
            </button>
        </div>
        <div class="cambodia__favorite--des mt-5">
            <div data-slide="0" class="d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Siem Reap located in the north western Cambodia is one of the two largest and most famous cities of this country, which attracts thousands of travelers to visit this city.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Siem Reap has an endearing landscape with mountains and rivers adapting harmoniously. The Tonle Sap which is a stream of great Mekong river flows through this city, which creates three unique living areas for people of this region. By visiting floating villages of Chong Kneas, Kampong Pluk, and Kampong Kleang, we could discover deeply the daily life of local people.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Kulen Mountain and Kbal Spean mountain are famous to the tourists for treacherous trekking route as well as the relics of thousands of lingas which is the symbol of worship and religion of Hinduism.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Kulen Mountain has been assumed as the place providing with stones to build the mightiest construction of Cambodia and South East Asia – the Angkor Complex. Angkor of Siem Reap is not a tower like a lot of people mistakenly believe, it is a great complex of Hinduism temples and towers built since the 12th century. This complex is archaeologically divided into the Small Circuit featured with the most well-known temple of Angkor Wat and Angkor Thom with Bayon, Ba Phuon temple, South Gate, Ta Prohm temple that need one full day to cover and Grand Circuit with over 26 kilometers long assembles the outer temples like Preak Khan, Neak Pean, Ta Som, East Meabon, Banteay Samre that need at least two to fours days to discover.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Angkor area was the ancient capital of Khmer Empire, from 9th – 15th century. This complex of temples and towers is the present that the Khmer King praised to the Gods, as well as boasted to the world about their ability and power. Travelling through Siem Reap, you could definitely fancy the peaceful lifestyle of local people, and be surprised with the timeless towers and overwhelmed because an Empire was extremely glorious in the past.</p>
            </div>
            <div data-slide="1" class="d-none d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">The Capital City of Phnom Penh, lying at the junction of the Mekong River is the biggest city of this country. It has been the capital of Cambodia since the 15th century, when the Khmer Empire moved their metropolis of kingdom from Angkor in Siem Reap to this area.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">While Siem Reap is affected strongly by the Hinduist features, Phnom Penh is marked profoundly by Buddhism, which expresses by numerous attractions of this city, typically the Wat Phnom Temple and Silver Pagoda. The reason for this difference is that when the Khmer Empire moved the capital here, they had gradually followed Buddhism, not strictly praising Hinduism like when they were in Angkor.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Cambodia is a sovereign country, with the King, the Queen and Royal Family still existing. That is the reason why the Royal construction is really popular in this city, representatively is the Royal Palace – the place considered as the symbol of Phnom Penh.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Besides the beautiful and precious attraction, Phnom Penh is also the place with the tragic killing fields restoring the skulls, real objects during the darkest Holocaust in Cambodia history, as known as the Khmer Rouge in the last century.</p>
                <p class="wt-fw-400 fst-italic wt-fz-20 wt-color-gray lh-sm text-center mb-3 px-md-5">Phnom Penh tells its stories through the relics and buildings sticking with the city’s history. Even though it was painful in the past, this city is still dazzling with the elegant constructions, and dynamic with the local market in the city center.</p>
            </div>
        </div>
        <div class="cambodia__favorite--control-other my-5 px-md-5">
            <div class="w-100 pt-3 px-3 d-flex justify-content-between">
                <button class="prev wt-font-merriweather fw-bold wt-fz-14 px-3 py-2 wt-rounded-6" type="button"><i class="fa-solid fa-arrow-left-long pr-2"></i> Previous</button>
                <button class="next wt-font-merriweather fw-bold wt-fz-14 px-3 py-2 wt-rounded-6" type="button">Next <i class="fa-solid fa-arrow-right-long pl-2"></i></button>
            </div>
        </div>
    </section>
</div>

@endsection
